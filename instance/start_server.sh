#!/bin/bash

echo Starting Server Update

FACTORIO_HOME=/home/factorio
FACTORIO_GIT_REPO=$FACTORIO_HOME/factorio-git

DOWNLOAD_FOLDER=$FACTORIO_HOME/downloads
if [ ! -d "$DOWNLOAD_FOLDER" ]; then
    mkdir $DOWNLOAD_FOLDER
fi

echo Downloading latests version
curl -L https://factorio.com/get-download/stable/headless/linux64 -o $DOWNLOAD_FOLDER/factorio_server.tar.xz

SERVER_FOLDER=$FACTORIO_HOME/server
if [ ! -d "$SERVER_FOLDER" ]; then
    mkdir $SERVER_FOLDER
fi

echo Extraction content
tar -xf $DOWNLOAD_FOLDER/factorio_server.tar.xz -C $SERVER_FOLDER --strip-components=1

DATA_FOLDER=$FACTORIO_HOME/data
if [ ! -d "$DATA_FOLDER" ]; then
    echo Creating data folder
    mkdir $DATA_FOLDER
fi

echo Updating configuration
cp $FACTORIO_GIT_REPO/instance/server-settings.json $DATA_FOLDER/server-settings.json
cp $FACTORIO_GIT_REPO/instance/map-settings.json $DATA_FOLDER/map-settings.json
cp $FACTORIO_GIT_REPO/instance/map-gen-settings.json $DATA_FOLDER/map-gen-settings.json

if [ ! -f "$DATA_FOLDER/save.zip" ]; then
    echo Save not found. Creating a new one.
    $SERVER_FOLDER/bin/x64/factorio --create $DATA_FOLDER/save.zip --map-gen-settings $DATA_FOLDER/map-gen-settings.json --map-settings $DATA_FOLDER/map-settings.json
fi

echo Starting Server

screen -d -m -L $SERVER_FOLDER/bin/x64/factorio --start-server $DATA_FOLDER/save.zip
