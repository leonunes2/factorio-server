import boto3
from botocore.config import Config


STEAM_INSTANCE_TAG_KEY = "Name"
STEAM_INSTANCE_TAG_VALUE = "FactorioInstance"


class EC2_Wrapper:
    def __init__(self):
        self._ec2_client = None

    def get_ec2_client(self):
        if not self._ec2_client:
            config = Config(
                region_name="sa-east-1"
            )
            credentials = self.get_credentials()
            self._ec2_client = boto3.client(
                'ec2',
                config=config,
                aws_access_key_id=credentials["aws_access_key_id"],
                aws_secret_access_key=credentials["aws_secret_access_key"],
            )
        return self._ec2_client

    def get_credentials(self):
        with open("credentials") as file:
            access_key = file.readline().replace('\n', '')
            secret_key = file.readline().replace('\n', '')
            return {
                "aws_access_key_id": access_key,
                "aws_secret_access_key": secret_key
            }

    def list_instances_metadata(self):
        ec2 = self.get_ec2_client()
        return ec2.describe_instances()["Reservations"][0]["Instances"]

    def list_instances(self):
        return [
            EC2Instance(md, self)
            for md
            in self.list_instances_metadata()
        ]

    def get_instance_metadata_by_id(self, id):
        ec2 = self.get_ec2_client()
        instances = ec2.describe_instances(
            Filters=[{"Name": "instance-id", "Values": [id]}]
        )

        if len(instances["Reservations"]) == 0:
            return None

        return instances["Reservations"][0]["Instances"][0]

    def get_instance_by_id(self, id):
        metadata = self.get_instance_metadata_by_id(id)
        return EC2Instance(metadata, self)

    def get_instances_metadata_by_tag(self, key, value):
        ec2 = self.get_ec2_client()
        result = ec2.describe_instances(
            Filters=[{"Name": f"tag:{key}", "Values": [value]}]
        )

        if len(result["Reservations"]) == 0:
            return None

        return [
            instance for reservation in result["Reservations"]
            for instance in reservation["Instances"]
        ]

    def get_instances_by_tag(self, key, value):
        return [
            EC2Instance(md, self)
            for md
            in self.get_instances_metadata_by_tag(key, value)
        ]

    def start_instance(self, id):
        ec2 = self.get_ec2_client()
        ec2.start_instances(
            InstanceIds=[id]
        )
        waiter = ec2.get_waiter("instance_running")
        waiter.wait(
            InstanceIds=[id]
        )

    def stop_instance(self, id):
        ec2 = self.get_ec2_client()
        ec2.stop_instances(
            InstanceIds=[id]
        )
        waiter = ec2.get_waiter("instance_stopped")
        waiter.wait(
            InstanceIds=[id]
        )


class EC2Instance:
    def __init__(self, metadata, ec2_wrapper):
        self._metadata = metadata
        self._ec2_wrapper = ec2_wrapper

    def id(self):
        return self._metadata["InstanceId"]

    def is_stopped(self):
        return self._metadata["State"]["Code"] == 80

    def is_running(self):
        return self._metadata["State"]["Code"] == 16

    def is_terminated(self):
        return self._metadata["State"]["Code"] == 48

    def public_ip(self):
        return self._metadata["PublicIpAddress"]

    def start(self):
        self._ec2_wrapper.start_instance(self.id())
        new_metadata = self._ec2_wrapper.get_instance_metadata_by_id(self.id())
        self.update_metadata(new_metadata)

    def stop(self):
        self._ec2_wrapper.stop_instance(self.id())
        new_metadata = self._ec2_wrapper.get_instance_metadata_by_id(self.id())
        self.update_metadata(new_metadata)

    def update_metadata(self, metadata):
        self._metadata = metadata


def start_steam_instance():
    ec2 = EC2_Wrapper()
    steam_instance = [
        i for i
        in ec2.get_instances_by_tag(
            STEAM_INSTANCE_TAG_KEY, STEAM_INSTANCE_TAG_VALUE
        )
        if not i.is_terminated()
    ][0]
    print(f"Id da isntancia: {steam_instance.id()}")
    if steam_instance.is_stopped():
        print("Starting the server")
        steam_instance.start()
        print("Server started")
        print(f"Public IP: {steam_instance.public_ip()}")
    else:
        print("Server is already started")
        print(f"Public IP: {steam_instance.public_ip()}")


def stop_steam_instance():
    ec2 = EC2_Wrapper()
    print(len(ec2.get_instances_metadata_by_tag(
        STEAM_INSTANCE_TAG_KEY, STEAM_INSTANCE_TAG_VALUE)))
    steam_instance = [
        i for i
        in ec2.get_instances_by_tag(
            STEAM_INSTANCE_TAG_KEY, STEAM_INSTANCE_TAG_VALUE
        )
        if not i.is_terminated()
    ][0]
    print(f"Id da isntancia: {steam_instance.id()}")
    if steam_instance.is_running():
        print("Stoping the server")
        steam_instance.stop()
        print("Server stopped")
    else:
        print("Server is already stoped")


if __name__ == "__main__":
    start_steam_instance()
