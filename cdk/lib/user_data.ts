import * as ec2 from '@aws-cdk/aws-ec2'

export const userData = ec2.UserData.custom(`Content-Type: multipart/mixed; boundary="//"
MIME-Version: 1.0

--//
Content-Type: text/cloud-config; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud-config.txt"

#cloud-config
cloud_final_modules:
- [scripts-user, always]

--//
Content-Type: text/x-shellscript; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="userdata.txt"

#!/bin/bash
FACTORIO_USER=factorio
echo $(date +%Y-%m-%d_%T) - Running startup scripts

useradd -m $FACTORIO_USER
FACTORIO_HOME=/home/$FACTORIO_USER

FACTORIO_GIT_REPO=$FACTORIO_HOME/factorio-git
if [ ! -d "$FACTORIO_GIT_REPO" ]; then
    git clone https://leonunes2@bitbucket.org/leonunes2/factorio-server.git $FACTORIO_GIT_REPO
fi
cd $FACTORIO_GIT_REPO
git pull

chown -R $FACTORIO_USER $FACTORIO_HOME

cd $FACTORIO_HOME
su -c "bash $FACTORIO_GIT_REPO/instance/start_server.sh" -m $FACTORIO_USER
--//
`);