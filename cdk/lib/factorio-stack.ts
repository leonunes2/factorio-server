import * as cdk from '@aws-cdk/core';
import * as ec2 from '@aws-cdk/aws-ec2'
import { userData } from './user_data';

export class FactorioStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const vpc = new ec2.Vpc(this, 'VPC', {
            maxAzs: 1,
            subnetConfiguration: [{
                name: "PublicSubnet",
                subnetType: ec2.SubnetType.PUBLIC
            }]
        });
        const securityGroup = new ec2.SecurityGroup(this, "FactorioSecurityGroup", {
            vpc,
            description: "Security Group for the Factorio Server",
            allowAllOutbound: true,
        });
        securityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(22), "SSH access");
        securityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.udp(34197), "Factorio UDP");
        securityGroup.addIngressRule(securityGroup, ec2.Port.allTraffic());

        const instance = new ec2.Instance(this, "FactorioInstance", {
            vpc,
            vpcSubnets: {
                subnetType: ec2.SubnetType.PUBLIC,
            },
            securityGroup,
            machineImage: ec2.MachineImage.genericLinux({
                "sa-east-1": "ami-0c3c87b7d583d618f"
            }, {
                userData
            }),
            instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
            keyName: "steamec2",
        });

        cdk.Tags.of(instance).add("Name", "FactorioInstance", {});
    }
}
