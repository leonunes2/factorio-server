#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { FactorioStack } from '../lib/factorio-stack';

const app = new cdk.App();
new FactorioStack(app, 'Factorio', {
    env: { region: 'sa-east-1' }
});
