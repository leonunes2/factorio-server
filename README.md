# Running Scripts

## Setup

### 1. Create a virtual environment

In the root folder of the project run:
```
$ python3 -m venv env
```

### 2. Activate environment

Linux:
```
$ source env/bin/activate
```

Windows:
```
$ env\Scripts\activate.bat
```

### 3. Install boto3

```
pip3 install boto3
```

### 4. Add credentials

Add the file named `credentials` in the project root

## Running

### 1. Activate environment

Linux:
```
$ source env/bin/activate
```

Windows:
```
$ env\Scripts\activate.bat
```

### 2. Run script

```
$ python3 scripts/start_server.py
```

or 

```
$ python3 scripts/stop_server.py
```

# CDK

Install typescript:
```
$ npm -g install typescript
```

Install CDK

```
$ npm install -g aws-cdk
```